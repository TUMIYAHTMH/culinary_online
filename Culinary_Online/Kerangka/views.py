from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

# Create your views here.
def Kerangka (request):
    Layout = loader.get_template('kerangka.html')
    return HttpResponse(Layout.render())