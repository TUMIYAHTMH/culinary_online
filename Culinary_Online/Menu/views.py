from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

# Create your views here.
def menuku (request):
    Layout = loader.get_template('detil.html')
    return HttpResponse(Layout.render())
    
 
def menu (request):
    template = loader.get_template('index.html')
    return HttpResponse(template.render())